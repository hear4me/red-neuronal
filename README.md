# Clasificador de audios

## Entrenamiento

### Parámetros

| Parámetro      | Tipo    | Valor default                | Descripción                                                                                                            |
| -------------- | ------- | ---------------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| num_epocas     | Integer | `1`                          | Cantidad de épocas.                                                                                                    |
| fine_tune      | Bool    | `False`                      | Si es True permite que los parámetros de VGGish cambien durante el entrenamiento.                                      |
| checkpoint     | Bool    | `False`                      | Cargar desde checkpoint temporal.                                                                                      |
| reset          | Bool    | `False`                      | Entrenar desde cero.                                                                                                   |
| earlystop      | Integer | `0`                          | Cantidad de veces que el loss aumenta antes de detener el entrenamiento.                                               |
| tag            | String  | `sin-tag`                    | Etiqueta para identificar los logs de la red.                                                                          |
| muestras_dir   | String  | `sonidos/casa/entrenamiento` | Directorio donde se encuentran las clases y los sonidos que se utilizaran para entrenar la red. Una carpeta por clase. |
| validacion_dir | String  | `sonidos/casa/validacion`    | Directorio donde se encuentran las clases y los sonidos que se utilizaran para validar la red. Una carpeta por clase.  |

### Ejemplo

Carga una instancia de la red con los pesos originales de AudioSet y entrena solo la red superficial por 30 épocas, usando las muestras de entrenamiento y validación de los directorios por defecto.

```bash
$ python3 entrenar.py --num_epocas 30 --reset
```

## Predicción

### Parámetros

| Parámetro       | Tipo   | Valor default | Descripción                     |
| --------------- | ------ | ------------- | ------------------------------- |
| archivo         | String | <vacio>       | Archivo a predecir              |
| usar_checkpoint | Bool   | `False`       | Usar los pesos de un checkpoint |
| usar_save       | Bool   | `True`        | Usar los pesos de un save       |

### Ejemplo

Carga una instancia de la red con los pesos guardados del último entrenamiento y clasifica el archivo `audio.wav`.

```bash
$ python3 predecir.py --archivo audio.wav
```

## Conversión

Después de haber entrenado la red al menos una vez se puede llamar al script para convertir de .h5 a .tflite de la siguiente manera:

```bash
$ python3 convertir.py
```