import socket
import base64
import json
import hashlib
import unittest

import predecir as clasificador

_HOST = '192.168.0.241'
_PORT = 14600

_ARCHIVO_TEMP = '/tmp/temp.wav'

class ServidorTest(unittest.TestCase):
  socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  conn = None
  addr = None

  def setUp(self):
    self.socket.bind((_HOST, _PORT))
    self.socket.listen(1)
    print('Esperando clientes...')
    self.conn, self.addr = self.socket.accept()
    print('Conectado: ' + self.addr[0] + ':' + str(self.addr[1]))

  def test_recibirString_stringCorrecto(self):
    entrada = ""
    while entrada.find('\n') == -1:
      entrada += self.conn.recv(1024).decode('utf-8')
    print('Input: ' + entrada)
    self.assertEqual(entrada.strip(), "caracteres")
  
  def test_enviarString(self):
    self.conn.sendall(b"caracteres\n")

  def tearDown(self):
    self.socket.close()

class ServidorRespuestaTest(unittest.TestCase):
  """
  Espera a que el cliente le escriba algo y lo devuelve
  """
  socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  conn = None
  addr = None
  
  def test_escucharConexiones(self):
    # Iniciar servidor
    self.socket.bind((_HOST, _PORT))
    self.socket.listen(1)
    print('Esperando clientes...')
    while 1:
      # Conectar cliente
      self.conn, self.addr = self.socket.accept()
      print('Conectado: ' + self.addr[0] + ':' + str(self.addr[1]))

      # Recibir string
      entrada = ""
      while entrada.find('\n') == -1:
        entrada += self.conn.recv(1024).decode('utf-8')
      print('Input: ' + entrada)

      # Reenviar string recibido
      self.conn.sendall(str.encode(entrada))
      
if __name__ == '__main__':
  unittest.main(warnings='ignore')