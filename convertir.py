"""
Convierte el archivo HDF5 (.h5) guardado a TensorFlow 
Lite FlatBuffer (.tflite)
"""

import tensorflow as tf

import os

import params
import utils

ultimo_save = utils.get_last_file_in(params.SAVE_PATH, '*')
converter = tf.lite.TFLiteConverter.from_keras_model_file(ultimo_save)
converter.post_training_quantize = True
tflite_model = converter.convert()
if (not os.path.isdir(params.TFLITE_DIR)):
  os.mkdir(params.TFLITE_DIR)
open(params.TFLITE_PATH, "wb+").write(tflite_model)