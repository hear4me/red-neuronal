import socket
import base64
import json
import hashlib
import time

import predecir as clasificador

def procesarInput(entrada):
  data = json.loads(entrada)
  audio = base64.b64decode(data['audio'])
  return audio, data['md5']

_HOST = '192.168.0.241'
_PORT = 14600

_ARCHIVO_TEMP = '/tmp/temp.wav'

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind((_HOST, _PORT))

s.listen(1)
print('Esperando clientes...')

while 1:
  conn, addr = s.accept()
  print('Conectado: ' + addr[0] + ':' + str(addr[1]))

  t0 = time.clock() # Conexion iniciada

  entrada = ""
  while entrada.find('\n') == -1:
    entrada += conn.recv(1024).decode('utf-8')
  audio, md5 = procesarInput(entrada)
  md5_real = hashlib.md5(audio).hexdigest()

  t1 = time.clock() # Termina de recibir string

  # if (md5_real != md5):
  #   print('Los hashes no coinciden. Cerrando conexión...'); break
  # clasificar audio y enviar resultado
  archivo = open(_ARCHIVO_TEMP, 'wb')
  archivo.write(audio)
  archivo.close()

  t2 = time.clock() # Termina de guardar audio en un archivo

  print('Clasificando...')
  preds, orden = clasificador.predecir(archivo=_ARCHIVO_TEMP)
  resultado = {
    'primero': { 'id': int(orden[0]), 'valor': float(preds[orden[0]]) },
    'segundo': { 'id': int(orden[1]), 'valor': float(preds[orden[1]]) },
    'tercero': { 'id': int(orden[2]), 'valor': float(preds[orden[2]]) }
  }

  t3 = time.clock() # Termina de clasificar audio

  print('Enviando resultados...')
  conn.sendall(str.encode(json.dumps(resultado) + '\n'))

  t4 = time.clock() # Termina de enviar clasificacion

  print('Tiempo de clasificacion (segundos): ' + str(t3 - t2))
  print('Tiempo total (segundos): ' + str(t4 - t0))

  conn.close()

s.close()