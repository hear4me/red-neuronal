import utils

ARCHIVO_WEIGHTS = 'weights/weights.h5' # Pesos originales

TEMP_CHECKPOINT_DIR = 'temp/' # Pesos durante entrenamiento

SAVE_PATH = 'save/'

SAVE_NAME = utils.get_formatted_datetime() + '.h5' # Model completo con arquitectura y pesos

TFLITE_DIR = 'tflite/'

TFLITE_PATH = 'tflite/clasificador_sonidos.tflite'

LOGS_DIR = 'logs/'

NOMBRE_ARCHIVO_ETIQUETA = 'label'

NUM_CLASES = 14

ETIQUETAS = {
  0:  [1,0,0,0,0,0,0,0,0,0,0,0,0,0],
  1:  [0,1,0,0,0,0,0,0,0,0,0,0,0,0],
  2:  [0,0,1,0,0,0,0,0,0,0,0,0,0,0],
  3:  [0,0,0,1,0,0,0,0,0,0,0,0,0,0],
  4:  [0,0,0,0,1,0,0,0,0,0,0,0,0,0],
  5:  [0,0,0,0,0,1,0,0,0,0,0,0,0,0],
  6:  [0,0,0,0,0,0,1,0,0,0,0,0,0,0],
  7:  [0,0,0,0,0,0,0,1,0,0,0,0,0,0],
  8:  [0,0,0,0,0,0,0,0,1,0,0,0,0,0],
  9:  [0,0,0,0,0,0,0,0,0,1,0,0,0,0],
  10: [0,0,0,0,0,0,0,0,0,0,1,0,0,0],
  11: [0,0,0,0,0,0,0,0,0,0,0,1,0,0],
  12: [0,0,0,0,0,0,0,0,0,0,0,0,1,0],
  13: [0,0,0,0,0,0,0,0,0,0,0,0,0,1]
}

ETIQUETAS_STR = {
  0:  'alarma_auto',
  1:  'alarma_incendio',
  2:  'ambulancia',
  3:  'bebe',
  4:  'bocina',
  5:  'bomberos',
  6:  'disparos',
  7:  'gato',
  8:  'perro',
  9:  'sirena_policia',
  10: 'silencio',
  11: 'timbre',
  12: 'vidrio',
  13: 'voz_humana',
}