import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

import os
import numpy as np
import time

from VGGish import vggish_input
from VGGish import vggish_params

from clasificador_sonidos import ClasificadorSonidos
import params
import utils

flags = tf.app.flags

flags.DEFINE_string(
  'archivo', '',
  'Archivo a predecir')

flags.DEFINE_boolean(
  'usar_checkpoint', False,
  'Usar los pesos de un checkpoint')

flags.DEFINE_boolean(
  'usar_save', True,
  'Usar los pesos de un save')

FLAGS = flags.FLAGS

def predecir(usar_checkpoint=False, usar_save=True, archivo=None):
  if (not archivo):
    print('No hay archivo que predecir'); return

  model = None
  if (usar_checkpoint):
    model = ClasificadorSonidos()
    model.load_weights(utils.get_last_file_in(
      params.TEMP_CHECKPOINT_DIR, '/**/checkpoint'))
  elif (usar_save):
    model = tf.keras.models.load_model(params.SAVE_PATH)

  optimizer = tf.train.AdamOptimizer(
    learning_rate=vggish_params.LEARNING_RATE,
    epsilon=vggish_params.ADAM_EPSILON)

  model.compile(optimizer=optimizer, 
    loss='categorical_crossentropy', 
    metrics=['accuracy'])
  
  t0 = time.clock()

  x = vggish_input.wavfile_to_examples(archivo)

  t1 = time.clock()

  print('Tiempo en preprocesar audio (seg): ' + str(t1 - t0))

  x = np.expand_dims(np.array(x), 3)
  predicciones = model.predict(x)

  t2 = time.clock()

  print('Tiempo en calcular predicciones (seg): ' + str(t2 - t1))

  pred_ordenadas = np.argsort(-predicciones[0])

  print('\nPredicciones:')
  print('1ro: %s (%.4f)' % (params.ETIQUETAS_STR[pred_ordenadas[0]], predicciones[0][pred_ordenadas[0]]))
  print('2do: %s (%.4f)' % (params.ETIQUETAS_STR[pred_ordenadas[1]], predicciones[0][pred_ordenadas[1]]))
  print('3ro: %s (%.4f)' % (params.ETIQUETAS_STR[pred_ordenadas[2]], predicciones[0][pred_ordenadas[2]]))

  return predicciones[0], pred_ordenadas


def main(_):
  predecir(usar_checkpoint=FLAGS.usar_checkpoint,
            usar_save=FLAGS.usar_save,
            archivo=FLAGS.archivo)

if __name__ == "__main__":
  tf.app.run()