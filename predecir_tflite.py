import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

import os
import numpy as np
import time

from VGGish import vggish_input
from VGGish import vggish_params

from clasificador_sonidos import ClasificadorSonidos
import params
import utils

flags = tf.app.flags

flags.DEFINE_string(
  'archivo', '',
  'Archivo a predecir')

FLAGS = flags.FLAGS

def predecir(archivo=None):
  if (not archivo):
    print('No hay archivo que predecir'); return

  model = None

  interpreter = tf.lite.Interpreter(model_path=params.TFLITE_PATH)
  interpreter.allocate_tensors()

  input_details = interpreter.get_input_details()
  output_details = interpreter.get_output_details()
  
  t0 = time.clock()

  x = vggish_input.wavfile_to_examples(archivo)

  t1 = time.clock()

  print('Tiempo en preprocesar audio (seg): ' + str(t1 - t0))

  x = np.expand_dims(np.array(x), 3)
  x = np.expand_dims(np.array(x[2]), 0)
  x = x.astype(np.float32)
  interpreter.set_tensor(input_details[0]['index'], x)

  interpreter.invoke()

  t2 = time.clock()

  print('Tiempo en calcular predicciones (seg): ' + str(t2 - t1))

  predicciones = interpreter.get_tensor(output_details[0]['index'])

  pred_ordenadas = np.argsort(-predicciones[0])

  print('\nPredicciones:')
  print('1ro: %s (%.4f)' % (params.ETIQUETAS_STR[pred_ordenadas[0]], predicciones[0][pred_ordenadas[0]]))
  print('2do: %s (%.4f)' % (params.ETIQUETAS_STR[pred_ordenadas[1]], predicciones[0][pred_ordenadas[1]]))
  print('3ro: %s (%.4f)' % (params.ETIQUETAS_STR[pred_ordenadas[2]], predicciones[0][pred_ordenadas[2]]))

  return predicciones[0], pred_ordenadas


def main(_):
  predecir(archivo=FLAGS.archivo)

if __name__ == "__main__":
  tf.app.run()