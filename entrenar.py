import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

import os
import numpy as np

from VGGish import vggish_input
from VGGish import vggish_params

from clasificador_sonidos import ClasificadorSonidos
import params
import utils

flags = tf.app.flags

flags.DEFINE_integer(
  'num_epocas', 1,
  'Cantidad de epocas.')

flags.DEFINE_boolean(
  'fine_tune', False,
  'Si es True permite que los parametros de VGGish cambien durante '
  'el entrenamiento.')

flags.DEFINE_boolean(
  'checkpoint', False,
  'Cargar desde checkpoint temporal')

flags.DEFINE_boolean(
  'reset', False,
  'Entrenar desde cero')

flags.DEFINE_integer(
  'earlystop', 0,
  'Cantidad de veces que el loss aumenta antes de detener '
  'el entrenamiento')

flags.DEFINE_string(
  'tag', 'sin-tag',
  'Etiqueta para identificar los logs de la red.')

flags.DEFINE_string(
  'muestras_dir', 'sonidos/casa/entrenamiento',
  'Directorio donde se encuentran las clases y los sonidos que '
  'se utilizaran para entrenar la red. Una carpeta por clase.')

flags.DEFINE_string(
  'validacion_dir', 'sonidos/casa/validacion',
  'Directorio donde se encuentran las clases y los sonidos que '
  'se utilizaran para validar la red. Una carpeta por clase.')

FLAGS = flags.FLAGS

def _get_muestras(muestras_dir):
  muestras = []
  for directorio in os.listdir(muestras_dir):
    # print(directorio)
    sonidos_dir = os.path.join(muestras_dir, directorio)
    with open(os.path.join(sonidos_dir, params.NOMBRE_ARCHIVO_ETIQUETA)) as f:
      etiqueta_id = int(f.readline().strip())
    for archivo in os.listdir(sonidos_dir):
      if (archivo != params.NOMBRE_ARCHIVO_ETIQUETA):
        # print("-- " + archivo)
        archivo_dir = os.path.join(sonidos_dir, archivo)
        for k in vggish_input.wavfile_to_examples(archivo_dir):
          muestras.append([k, params.ETIQUETAS[etiqueta_id]])

  caracteristicas = [c for c,_ in muestras]
  etiquetas = [e for _,e in muestras]

  caracteristicas = np.expand_dims(np.array(caracteristicas), 3)
  etiquetas = np.array(etiquetas)

  return caracteristicas, etiquetas

def main(_):

  model = None
  ultimo_save = utils.get_last_file_in(
      os.path.join(params.SAVE_PATH, FLAGS.tag), '*')
  if (FLAGS.reset):
    model = ClasificadorSonidos(freeze_base=FLAGS.fine_tune)
  elif (FLAGS.checkpoint):
    model = ClasificadorSonidos(freeze_base=FLAGS.fine_tune)
    model.load_weights(utils.get_last_file_in(
      params.TEMP_CHECKPOINT_DIR, '/**/checkpoint'))
  elif (ultimo_save != None):
    model = tf.keras.models.load_model(ultimo_save)

  features, labels = _get_muestras(FLAGS.muestras_dir)
  val_x, val_y = _get_muestras(FLAGS.validacion_dir)

  optimizer = tf.train.AdamOptimizer(
    learning_rate=vggish_params.LEARNING_RATE,
    epsilon=vggish_params.ADAM_EPSILON)

  model.compile(optimizer=optimizer, 
    loss='categorical_crossentropy', 
    metrics=['accuracy'])

  log_dir = os.path.join(params.LOGS_DIR, FLAGS.tag, utils.get_formatted_datetime())

  tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

  checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    os.path.join(params.TEMP_CHECKPOINT_DIR, utils.get_formatted_datetime(), 'checkpoint'), 
    save_weights_only=True,
    verbose=1, period=5)

  callbacks = [tensorboard_callback, checkpoint_callback]

  if (FLAGS.earlystop != 0):
    callbacks += tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=FLAGS.earlystop)

  model.fit(x=features, y=labels,
    validation_data=(val_x, val_y), epochs=FLAGS.num_epocas, shuffle=True,
    callbacks=callbacks)
  
  model.save(os.path.join(params.SAVE_PATH, FLAGS.tag, params.SAVE_NAME))
  

if __name__ == "__main__":
  tf.app.run()