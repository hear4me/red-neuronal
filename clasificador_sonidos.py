import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout

from VGGish import vggish

import params

def ClasificadorSonidos(freeze_base=False):
  base_model = vggish.VGGish()

  if (freeze_base):
    for layer in base_model.layers:
      layer.trainable = False

  x = base_model.output
  x = Dense(128, activation='relu')(x)
  x = Dropout(0.5)(x)
  x = Dense(64, activation='relu')(x)
  x = Dropout(0.3)(x)
  x = Dense(params.NUM_CLASES, activation='sigmoid')(x)

  model = tf.keras.Model(inputs=base_model.inputs, outputs=x)

  return model

if __name__ == "__main__":
  ClasificadorSonidos(freeze_base=True).summary()