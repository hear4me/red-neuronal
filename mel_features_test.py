import unittest

import numpy as np
import numpy.testing as npt

import soundfile as sf

from VGGish import vggish_input as vinput

from VGGish import mel_features as mel

"""
Los tests que terminan en 'pass' son para inspeccionar en debug
o falta completarlos
"""
class MelFeaturesTest(unittest.TestCase):

  def test_periodicHann_inputValido(self):
    esperado = np.array([0, 0.095, 0.345, 0.654, 0.904, 1, 0.904, 0.645, 0.345, 0.095])
    resultado = mel.periodic_hann(10)
    npt.assert_allclose(resultado, esperado, 0.1)

  def test_frame_inputValidoArray_1(self):
    data = np.array([0, 0.1, 0.2, 0.3, 0.4])
    resultado = mel.frame(data, 2, 1)
    esperado = np.array([ [0, 0.1] ,[0.1, 0.2], [0.2, 0.3], [0.3, 0.4] ])
    npt.assert_allclose(resultado, esperado, 0.1)

  def test_frame_inputValidoArray_2(self):
    data = np.array([0, 0.1, 0.2, 0.3, 0.4])
    resultado = mel.frame(data, 3, 2)
    esperado = np.array([ [0, 0.1, 0.2], [0.2, 0.3, 0.4] ])
    npt.assert_allclose(resultado, esperado, 0.1)

  def test_frame_inputValidoMatriz(self):
    esperado = np.array([[[1,2], [3,4]], [[3,4], [5,6]] ])
    data = np.array([ [1,2], [3,4], [5,6] ])
    resultado = mel.frame(data, 2, 1)
    npt.assert_allclose(resultado, esperado, 0.1)

  def test_windowedFrames_inputValido(self):
    esperado = np.array([ [0, 0.1] ,[0, 0.2], [0, 0.3], [0, 0.4] ])
    frames = np.array([ [0, 0.1] ,[0.1, 0.2], [0.2, 0.3], [0.3, 0.4] ])
    window = np.array([0, 1])
    resultado = frames * window
    npt.assert_allclose(resultado, esperado, 0.1)

  def test_hertzToMel_inputValido(self):
    esperado = np.array([2840.03, 2903.01, 2962.65])
    resultado = mel.hertz_to_mel(np.array([8000, 8500, 9000]))
    npt.assert_allclose(resultado, esperado, 0.1)

  def test_spectrogramToMelMatrix_inputValido(self):
    resultado = mel.spectrogram_to_mel_matrix(10, 10, 16000, 0, 7500)
    pass

  def test_fft_inputValidoPar(self):
    data = np.array([0, 0.1, 0.2, 0.3])
    resultado = np.fft.rfft(data, data.size)
    pass

  def test_fft_inputValidoImpar(self):
    data = np.array([0, 0.1, 0.2, 0.3, 0.4])
    resultado = np.fft.rfft(data, data.size)
    pass
  
  def test_abs_inputComplejos(self):
    complejos = np.array([ complex(0.1, 0.2), complex(0.3, 0.4) ])
    resultado = np.abs(complejos)
    pass

  def test_stftMagnitude_inputValido(self):
    data = np.array([0, 0.1, 0.2, 0.3, 0.4])
    resultado = mel.stft_magnitude(data, 10, 1, 2)
    pass

  def test_archivo(self):
    archivo = './sonidos/v_casa/perro_ladrando/perro_ladrando_1.wav'
    wav_data, sr = sf.read(archivo, dtype='int16')
    pass
