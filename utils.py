import os
import glob
import datetime

def get_last_file_in(path, match = ''):
  """
  Args:
    path: Directorio en el que se va a buscar el archivo

  Returns:
    Path del ultimo archivo creado en el directorio o None
  """
  files = glob.glob(path + match)
  if not files:
      return None
  file_path = max(files, key=os.path.getctime)
  return file_path

def get_formatted_datetime():
  return datetime.datetime.now().strftime("%Y%m%d-%H%M%S")